var viewer;

// @urn the model to show
// @viewablesId which viewables to show, applies to BIM 360 Plans folder
function launchViewer(urn, vid, viewableId) {
  var options = {
    env: "AutodeskProduction",
    getAccessToken: getForgeToken,
  };

  Autodesk.Viewing.Initializer(options, () => {
    viewer = new Autodesk.Viewing.GuiViewer3D(
      document.getElementById("forgeViewer"),
      { extensions: ["Autodesk.DocumentBrowser"] }
    );

    viewer.start();

    var documentId = "urn:" + urn;
    Autodesk.Viewing.Document.load(
      documentId,
      onDocumentLoadSuccess,
      onDocumentLoadFailure
    );
  });

  function onDocumentLoadSuccess(doc) {
    // if a viewableId was specified, load that view, otherwise the default view
    var viewables = viewableId
      ? doc.getRoot().findByGuid(viewableId)
      : doc.getRoot().getDefaultGeometry();

    // var options = { ids: [], skipPropertyDb: true };

    viewer.loadDocumentNode(doc, viewables).then((i) => {
      // any additional action here?
    });

    viewer
      .waitForLoadDone()
      .then((s) => {
        let comps = vid.length;
        let counter = 0;

        viewer.setGhosting(false);
        setTimeout(() => {
          // Wait 2secs after the load is complete before extracting thumbnails
          let loop = setInterval(() => {
            // Wait 5secs before taking a new screenshot
            viewer.isolate(vid[counter]); // Isolate and display only one
            viewer.fitToView(vid[counter]); // Set to full screen

            if (counter == comps) {
              clearInterval(loop);
              console.log("Done");
            } else {
              console.log(counter + " - " + vid[counter]);
              viewer.getScreenShot(400, 400, (res) => {
                let blob = new Blob([res], { type: "image/png" });
                let reader = new FileReader();
                reader.readAsDataURL(blob); // converts the blob to base64 and calls onload
                // reader.onload = function (e) {
                //   link.href = res;
                // };
                var xhr = new XMLHttpRequest();
                xhr.responseType = "blob";
                xhr.onload = () => {
                  let recoveredBlob = xhr.response;
                  // console.log(xhr.response);
                  var reader = new FileReader();
                  reader.onload = () => {
                    let blobAsDataUrl = reader.result;
                    // Alternatively, async await could be used to send the data/thumbnail to the server.
                    axios
                      .post("/images/", {
                        imgUrl: blobAsDataUrl,
                        componentId: vid[counter],
                      })
                      .then((result) => {
                        counter++;
                        console.log(result);
                      })
                      .catch((err) => console.log(err));
                  };
                  reader.readAsDataURL(recoveredBlob);
                };
                xhr.open("GET", res);
                xhr.send();
              });
            }
          }, 5000);
        }, 2000);
      })
      .catch((err) => console.log(err));
  }

  function onDocumentLoadFailure(viewerErrorCode, viewerErrorMsg) {
    alert("Error... Slow network detected, please try again.");
    console.error(
      "onDocumentLoadFailure() - errorCode:" +
        viewerErrorCode +
        "\n- errorMessage:" +
        viewerErrorMsg
    );
    location.reload();
  }
}

function getForgeToken(callback) {
  fetch("/api/forge/oauth/token").then((res) => {
    res.json().then((data) => {
      callback(data.access_token, data.expires_in);
    });
  });
}

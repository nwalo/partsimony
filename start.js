require("dotenv").config();
const path = require("path");
const express = require("express");
const cookieSession = require("cookie-session");
const mongoose = require("mongoose");
const cors = require("cors");
const bodyParser = require("body-parser");
const session = require("express-session");
// const ejs = require("ejs"); // Not necessary only used as templating engine to render to the page saved data.

const PORT = process.env.PORT || 3000;
const config = require("./config.js");
if (
  config.credentials.client_id == null ||
  config.credentials.client_secret == null
) {
  console.error(
    "Missing FORGE_CLIENT_ID or FORGE_CLIENT_SECRET env. variables."
  );
  return;
}

// Mongoose Database connection.............
mongoose.connect("mongodb://localhost:27017/newPartsimonyDB", {
  useNewUrlParser: true,
  useUnifiedTopology: true,
});

// Schema Definition..................
const componentSchema = new mongoose.Schema({
  key: String, // identifier to relate the component and the metadata
  name: String, // name of the design
  urn: String, // used to view the model
  img: String,
  objectId: Number, // object id
  objects: [], // components of the design
});

const singleComponentSchema = new mongoose.Schema({
  name: String,
  externalId: String,
  objectid: String,
  properties: {},
  img: String,
});

const metadataSchema = new mongoose.Schema({
  key: String,
  type: String,
  component: [singleComponentSchema],
});

// Model Definition .....................
const Component = mongoose.model("Component", componentSchema);
const Metadata = mongoose.model("Metadata", metadataSchema);

// Express Middleware Definition ................
let app = express();
app.use(express.static(path.join(__dirname, "public")));
app.use(bodyParser.urlencoded({ extended: true }));
app.use("/uploads", express.static("uploads"));
app.use(cors());
app.set("view engine", "ejs");
app.use(
  cookieSession({
    name: "forge_session",
    keys: ["forge_secure_key"],
    maxAge: 3 * 24 * 60 * 60 * 1000, // 3 days, same as refresh token
  })
);
app.use(
  session({
    secret: process.env.SECRET,
    resave: false,
    saveUninitialized: false,
  })
);
app.use(express.json({ limit: "100mb" }));
app.use("/api/forge", require("./routes/oauth"));
app.use("/api/forge", require("./routes/datamanagement"));
app.use("/api/forge", require("./routes/user"));
app.use("/api/forge", require("./routes/jobs"));
app.use((err, req, res, next) => {
  console.error(err);
  res.status(err.statusCode).json(err);
});

let data = {};

// Main/Starting route, serving as the root route for this app (if you change the name, you need to update the url in oauth.js as well.)
// while testing load up localhost:3000/autodesk/project
app.get("/autodesk/project/", (req, res) => {
  res.sendFile("index.html", { root: path.join(__dirname, "public") });
});

// Component Hierarchy extraction........
app.post("/post/partsimony/component", (req, res) => {
  let result = req.body.data.objects;
  let comp;
  let compId;
  let compName;
  let output = [];

  let resObjects = result
    .map((i) => {
      return i.objects;
    })
    .flat(Infinity);

  result.forEach((element) => {
    compObjectId = element.objectid;
  });

  let loop = (array, parent) => {
    for (let i = 0; i < array.length; i++) {
      let p = array[i].objectid ? array[i].objectid : array[i].objectId;
      if (array[i].objects) {
        array[i].parent = parent;
        loop(array[i].objects, p);
      } else {
        array[i].parent = parent;
      }
    }
  };

  loop(resObjects, compObjectId);

  let componentKey;
  result.forEach((i) => {
    let word = i.name;
    word = word.replaceAll(" ", "");
    let array = word.split("");
    for (let index = 0; index < array.length; index++) {
      let next = index + 1;
      let matches = array[next].match(/\d+/g);
      if (matches != null) {
        componentKey = array.splice(0, index);
        break;
      }
    }

    req.session.key = componentKey.join("");
    comp = new Component({
      key: req.session.userId + "-" + req.session.key, // Use a unique identifier to create a key e.g firstName-lastName-designName.  You can use something much more unique than this.
      name: i.name,
      objectId: i.objectid,
      urn: i.urn,
      objects: resObjects,
    });

    compId = i.objectid;
    compName = i.name;
  });

  // Output component that would be saved to the database........
  // console.log(comp);
  // res.json({ status: true }); // Remove this when you want to want to save. this response initalizes the metadata extraction.
  // Saving the component to the database..........
  // Component.findOne(
  //   {
  //     objectId: compId,
  //     name: compName,
  //     key: req.session.userId + "-" + req.session.key.replaceAll(" ", "-"),
  //   },
  //   function (err, found) {
  //     if (err) {
  //       console.log("err");
  //     } else {
  //       if (found) {
  //         // Update the component if found
  //         Component.findOneAndUpdate(
  //           {
  //             key:
  //               req.session.userId + "-" + req.session.key.replaceAll(" ", "-"),
  //           },
  //           { objects: resObjects },
  //           (err) => {
  //             if (err) {
  //               res.send({ status: 500, response: err });
  //             } else {
  //               res.json({
  //                 status: true,
  //                 response: "Component was seen, hence updated",
  //               }); //!!!IMPORTANT Sends response, if true starts extraction of metadata......
  //               console.log("updated component");
  //             }
  //           }
  //         );
  //       } else {
  //         // Create a new component if not found.
  //         Component.create(comp, function (err, comps) {
  //           if (!err) {
  //             comps.save(function (err) {
  //               if (!err) {
  //                 res.json({
  //                   status: true,
  //                   response: "New component heirarchy has been saved",
  //                 }); //!!!IMPORTANT Sends response, if true starts extraction of metadata......
  //                 console.log("saved component");
  //               } else {
  //                 console.log("didn't save");
  //                 res.send({ status: 500, response: err });
  //               }
  //             });
  //           }
  //         });
  //       }
  //     }
  //   }
  // );
});

// Metadata extraction ..........
app.post("/post/partsimony/metadata/properties", (req, res) => {
  data = req.body.data;

  let meta = new Metadata({
    key: req.session.userId + "-" + req.session.key.replaceAll(" ", "-"),
    type: data.type,
    component: data.collection,
  });

  // Save Metadata to the database..........
  // Metadata.findOne(
  //   { key: req.session.userId + "-" + req.session.key.replaceAll(" ", "-") },
  //   (err, found) => {
  //     if (err) {
  //       console.log("err");
  //       res.send({ status: 500, response: err });
  //     } else {
  //       if (!found) {
  //         //Save new metadata if no component/ metadata exist
  //         meta.save((err) => {
  //           if (!err) {
  //             res.json({
  //               status: 200,
  //               data: data,
  //               response: "New metadata has been obtained and saved",
  //             }); // You can send any response of your choice to the client.
  //             console.log("saved metadata");
  //           } else {
  //             res.send({ status: 500, response: err });
  //             return console.log(err);
  //           }
  //         });
  //       } else {
  //         // Update metadata if component/metadata was found
  //         Metadata.findOneAndUpdate(
  //           {
  //             key:
  //               req.session.userId + "-" + req.session.key.replaceAll(" ", "-"),
  //           },
  //           { component: data.collection },
  //           (err) => {
  //             if (err) {
  //               res.send({ status: 500, response: err });
  //             } else {
  //               res.json({
  //                 status: 200,
  //                 data: data,
  //                 response: "Metadata already exist, hence it was updated.",
  //               }); //!!!IMPORTANT Sends response, if true starts extraction of metadata......
  //               console.log("updated metadata");
  //             }
  //           }
  //         );
  //       }
  //     }
  //   }
  // );
  // res.json(data); // Comment or remove this line when you are trying to save, it is a trigger for the below API..............
});

// This is triggered when a metadata is sent to the data base the api is consumed in forgetree.js line 236
app.get("/partsimony/get/metadata/properties", (req, res) => {
  // res.send(data);
  // Do anything you want here........
  res.send("Welcome to Partsimony Supply chain");
});

// To retrieve metadata ..........
// Not useful to the app, it is a way to view the components of a model as well as the properties
// app.get("/retrieve/properties/:key", (req, res) => {

//   Metadata.findOne({ key: req.params.key }, (err, found) => {
//     if (err) {
//       console.log("err");
//     } else {
//       if (found) {
//         let output = found.component.filter((comp) => {
//           return comp.img;
//         });
//         res.render("display", { data: output });
//       } else {
//         res.render("display", { data: [] });
//       }
//     }
//   });
// });

// End point that does the attachment of Thumbnails to the component.........................
app.post("/images", (req, res) => {
  // let key = "Nwalo-Okechukwu-flange-v3-1";
  let key = req.session.userId + "-" + req.session.key.replaceAll(" ", "-");
  let image = req.body.imgUrl;
  let componentId = req.body.componentId;

  Metadata.findOneAndUpdate(
    { key: key, "component.objectid": componentId },
    { $set: { "component.$.img": image } },
    (err, found) => {
      err ? console.log(err) : console.log("err");
    }
  );
  res.send({ status: 200 });
});

app.listen(PORT, () => {
  console.log(`Server listening on port ${PORT}`);
});
